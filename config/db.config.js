const mysql = require('mysql');

// create mysql connection
const dbConnect = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_binar'
});

dbConnect.connect(function (error) {
    if (error) throw error;
    // console.log('Database Connected Success!!!');
})

module.exports = dbConnect;