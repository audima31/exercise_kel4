const {
    threadId
} = require('../../config/db.config');
let dbConnect = require('../../config/db.config');

//membuat object dari databse
let Employee = function (employee) {
    this.employee_id = employee.employee_id;
    this.employeeName = employee.employeeName;
    this.employeeCity = employee.employeeCity;
    this.Position = employee.Position;
}

//get all employees
Employee.getAllEmployees = (result) => {
    dbConnect.query('SELECT * FROM employees', (err, res) => {
        if (err) {
            console.log('Error dalam mengambil data pegawai', err);
            result(null, err);
        } else {
            console.log('Data pegawai berhasil diambil');
            result(null, res);
        }
    })
}

//get employees data by id
Employee.getEmployeesByID = (id, result) => {
    dbConnect.query('SELECT * FROM employees WHERE employee_id = ?', id, (err, res) => {
        if (err) {
            console.log(`Error dalam mencari data pegawai by id`, err);
            result(null, err);
        } else {
            console.log('Data pegawai by ID berhasil diambil');
            result(null, res);
        }
    })
}

//create new data employee
Employee.createEmployee = (employeeReqData, result) => {
    dbConnect.query('INSERT INTO employees SET ? ', employeeReqData, (err, res) => {
        if (err) {
            console.log(`Error dalam memasukan data baru`, err);
            result(null, err);
        } else {
            console.log('Data pegawai berhasil diisi');
            result(null, res);
        }
    })
};

//update data employee
Employee.updateEmployee = (id, employeeReqData, result) => {
    dbConnect.query("UPDATE employees SET employeeName=?, employeeCity=?, Position=? WHERE employee_id = ?", [employeeReqData.employeeName, employeeReqData.employeeCity, employeeReqData.Position, id], (err, res) => {
        if (err) {
            console.log(`Error dalam mengupdate data baru`, err);
            result(null, err);
        } else {
            console.log('Data pegawai berhasil diupdate');
            result(null, res);
        }
    });
}

//delete data employee
Employee.deleteEmployee = (id, result) => {
    dbConnect.query('UPDATE employees SET is_deleted=? WHERE employee_id =?', [1, id], (err, res) => {
        if (err) {
            console.log(`Data gagal di delete`, err);
            result(null, err);
        } else {
            console.log('Data telah dihapus!!');
            result(null, res);
        }
    })
}




module.exports = Employee;