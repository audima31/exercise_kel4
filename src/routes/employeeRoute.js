const express = require('express');
const router = express.Router();

const employeeControllers = require('../controllers/employeeControllers');

//get employee all data
router.get('/', employeeControllers.getEmployeelist);

//get employee by id
router.get('/:id', employeeControllers.getEmployeeByID);

//create employee
router.post('/', employeeControllers.createNewEmployee);

//update employee
router.put('/:id', employeeControllers.updateEmployee);

//delete employee
router.delete('/:id', employeeControllers.deleteEmployee);




module.exports = router;