//Import dari
const Employee = require('../models/employeeModel');
const EmployeeModel = require('../models/employeeModel');

//get employee all data
exports.getEmployeelist = (req, res) => {
    EmployeeModel.getAllEmployees((err, employees) => {
        console.log('Data di controllers');
        if (err)
            res.send(err);
        console.log('Employees', employees);
        res.send(employees);

    });
};

//get employee by id
exports.getEmployeeByID = (req, res) => {
    EmployeeModel.getEmployeesByID(req.params.id, (err, employee) => {
        console.log('Data di controllers');
        if (err)
            res.send(err);
        console.log('Employee:', employee);
        res.send(employee);
    });
};

//create employee
exports.createNewEmployee = (req, res) => {
    console.log('Create data', req.body);
    const employeeReqData = new Employee(req.body);
    //Check data
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({
            success: false,
            message: "Data belum terisi"
        });
    } else {
        EmployeeModel.createEmployee(employeeReqData, (err, employee) => {
            if (err)
                res.send(err);
            res.json({
                status: true,
                message: 'Data berhasil ditambah',
                data: employee
            });
        });
    };
};

//update data employee
exports.updateEmployee = (req, res) => {
    const employeeReqData = new Employee(req.body);
    console.log('update data : ', employeeReqData);
    //Check data
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.send(400).send({
            success: false,
            message: "Data belum terisi"
        });
    } else {
        EmployeeModel.updateEmployee(req.params.id, employeeReqData, (err, employee) => {
            if (err)
                res.send(err);
            res.json({
                status: true,
                message: 'Data berhasil diupdate',
                data: employee
            });
        });
    };
};

//delete data employee
exports.deleteEmployee = (req, res) => {
    EmployeeModel.deleteEmployee(req.params.id, (err, employee) => {
        if (err)
            res.send(err);
        res.json({
            status: true,
            message: 'Data berhasil dihapus!',
        });
    })
}