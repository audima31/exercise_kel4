const express = require('express');
const bp = require('body-parser');

//import employeeRoute
const employeeRoute = require('./src/routes/employeeRoute')

//create express app
const app = express();

//setup server port
const port = process.env.PORT || 3008;

//parse request (ngambil dari postman) data content type application/x-www-form-rulencoded
app.use(bp.urlencoded({
    extended: false
}));

//parse request (ngambil dari postman) data content type application/JSON
app.use(bp.json());

//Create employeeroutes
app.use("/api/employee", employeeRoute);





//Listen
app.listen(port, () => {
    console.log(`Server berjalan http://localhost:${port}`);
})